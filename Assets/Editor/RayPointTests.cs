﻿using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class RayPointTests {

    [Test]
    public void BasicRayGenerationTest()
    {
        var second = GameUtility.CreateTile(_core, 0, 0).IsDirectionTile(true).AddDistance(1).AddRotation(Rotation.North);
        var third = GameUtility.CreateTile(_core, 0, 1).IsDirectionTile(true).AddDistance(2).AddRotation(Rotation.East);
        var fourth = GameUtility.CreateTile(_core, 2, 1).IsDirectionTile(true).AddDistance(1).AddRotation(Rotation.South);
        var fifth = GameUtility.CreateTile(_core, 2, 0).IsDirectionTile(true).AddDistance(1).AddRotation(Rotation.East);

        var first = GameUtility.CreateTile(_core, 0, -1).IsStartTile(true).AddDistance(1).AddRotation(Rotation.North);

        var pools = new Pools();
        pools.core = _core;

        var rayPoints = VisualizeRaySystem.CalculateRayEntities(_core.GetEntities(Matcher.AllOf(CoreMatcher.Tile, CoreMatcher.Position)), first);
        var expected = new[]
                       {
                           first,
                           second,
                           third,
                           fourth,
                           fifth
                       };


        CollectionAssert.AreEqual(expected, rayPoints);
    }

    [Test]
    public void FindRayEntitiesWithPortalsTest()
    {
        var first = GameUtility.CreateTile(_core, 0, -1).IsStartTile(true).AddDistance(1).AddRotation(Rotation.North);
        var second = GameUtility.CreateTile(_core, 0, 0).IsDirectionTile(true).AddDistance(1).AddRotation(Rotation.North);
        var third = GameUtility.CreateTile(_core, 0, 1).IsDirectionTile(true).AddDistance(1).AddRotation(Rotation.West);

        var fourth = GameUtility.CreateTile(_core, -1, 1);
        var fifth = GameUtility.CreateTile(_core, 1, 0).IsPortalTile(true);
        fourth.AddConnectedTiles(new List<Entity>() {fifth});

        var rayPoints = VisualizeRaySystem.CalculateRayEntities(_core.GetEntities(Matcher.AllOf(CoreMatcher.Tile, CoreMatcher.Position)), first);
        var expected = new[]
                       {
                           first,
                           second,
                           third,
                           fourth,
                          // fifth
                       };


        CollectionAssert.AreEqual(expected, rayPoints);
    }

    [Test]
    public void CalculateRayWithPortals()
    {
        var first = GameUtility.CreateTile(_core, 0, -1).IsStartTile(true).AddDistance(1).AddRotation(Rotation.North);
        var second = GameUtility.CreateTile(_core, 0, 0).IsDirectionTile(true).AddDistance(1).AddRotation(Rotation.East);
        var third = GameUtility.CreateTile(_core, -1, 0).IsDirectionTile(true).AddDistance(1).AddRotation(Rotation.East);

        var rightPortal = GameUtility.CreateTile(_core, -2, 0).IsPortalTile(true);
        var leftPortal = GameUtility.CreateTile(_core, 1, 0).IsPortalTile(true);
        rightPortal.AddConnectedTiles(new List<Entity>() { leftPortal });
        leftPortal.AddConnectedTiles(new List<Entity>() { rightPortal });
       
      
        var entities = VisualizeRaySystem.CalculateRayEntities(_core.GetEntities(Matcher.AllOf(CoreMatcher.Tile, CoreMatcher.Position)), first);
        var raySegments = VisualizeRaySystem.DropToSegments(entities);

        var expected = new[]
                       {
                           new Vector3(0, -1),
                           new Vector3(0, 0),
                           new Vector3(1, 0),
                       };
        var expectedLast = new[]
                           {
                               new Vector3(-2, 0),
                               new Vector3(-1, 0),
                               new Vector3(0,0)
                           };
        Assert.AreEqual(2, raySegments.Count);
        CollectionAssert.AreEqual(expected, raySegments.First());
        CollectionAssert.AreEqual(expectedLast, raySegments.Last());
    }

    private Pool _core;

    [SetUp]
    public void SetUp()
    {
        _core = new Pool
            (
            CoreComponentIds.TotalComponents,
            0,
            new PoolMetaData("test core", CoreComponentIds.componentNames, CoreComponentIds.componentTypes));
    }

    [Test]
    public void CalculateIntersection()
    {
        var point1 = new Vector2(0, 0);
        var point2 = new Vector2(0, 2);

        var point3 = new Vector2(-1, 1);
        var point4 = new Vector2(1, 1);

        var expected = new Vector2(0,1);
        var hasIntersection = GameUtility.IsLinesCross(point1, point2, point3, point4);
        Assert.IsTrue(hasIntersection);
        Vector2 intersection = GameUtility.Intersection(point1, point2, point3, point4);
        Assert.AreEqual(expected, intersection);
    }

    [Test]
    public void CalculateNoIntersection()
    {
        var point1 = new Vector2(0, 5);
        var point2 = new Vector2(0, 6);

        var point3 = new Vector2(-1, 1);
        var point4 = new Vector2(1, 1);

        var expected = new Vector2(0, 1);
        var hasIntersection = GameUtility.IsLinesCross(point1, point2, point3, point4);
        Assert.IsFalse(hasIntersection);
        Vector2 intersection = GameUtility.Intersection(point1, point2, point3, point4);
        Assert.AreEqual(expected, intersection);
    }

    [Test]
    public void SegmentMarkeringTest()
    {
        var points = new[]
                     {
                         new Vector3(0, 0),
                         new Vector3(0, 1),
                         new Vector3(2, 1),
                         new Vector3(2, 0),
                         new Vector3(1, 0),
                         new Vector3(1, 2),
                     };

        var segments = GameUtility.MarkSegments(points);

        var expectedSegments = new[]
                               {
                                   new Segment(0, SegmentType.Regular, new Vector2(0, 0), new Vector2(0, 1)),
                                   new Segment(1, SegmentType.Under, new Vector2(0, 1), new Vector2(2, 1)),
                                   new Segment(2, SegmentType.Regular, new Vector2(2, 1), new Vector2(2, 0)),
                                   new Segment(3, SegmentType.Regular, new Vector2(2, 0), new Vector2(1, 0)),
                                   new Segment(4, SegmentType.Top, new Vector2(1, 0), new Vector2(1, 2)),
                               };

        CollectionAssert.AreEqual(expectedSegments, segments);
    }

    [Test]
    public void SegmentIntersectionInfo()
    {
        var segment1 = new Segment(0, SegmentType.Regular, new Vector2(0, 0), new Vector2(2, 0));
        var segment2 = new Segment(1, SegmentType.Regular, new Vector2(1,-1), new Vector2(1, 1));

        var intersection = GameUtility.FindIntersection(segment1, segment2);
        Assert.IsTrue(intersection.HasValue);
        Assert.AreEqual(new Vector2(1,0), intersection.Value);
    }

    [Test]
    public void SegmentIntersectionInfoNo()
    {
        var segment1 = new Segment(0, SegmentType.Regular, new Vector2(1, 0), new Vector2(1, 2));
        var segment2 = new Segment(1, SegmentType.Regular, new Vector2(2, 0), new Vector2(1, 0));

        var intersection = GameUtility.FindIntersection(segment1, segment2);
        Assert.IsFalse(intersection.HasValue);
    }

    [Test]
    public void SegmentIntersectionInfoNoWithBorders()
    {
        var segment1 = new Segment(0, SegmentType.Regular, new Vector2(1, 0), new Vector2(1, 2));
        var segment2 = new Segment(1, SegmentType.Regular, new Vector2(2, 0), new Vector2(1, 0));

        var intersection = GameUtility.FindIntersection(segment1, segment2, true);
        Assert.IsTrue(intersection.HasValue);
    }
}