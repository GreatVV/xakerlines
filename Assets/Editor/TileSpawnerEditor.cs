﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TileSpawner))]
public class TileSpawnerEditor : Editor {

    void OnSceneGUI()
    {
        ListenToPress();
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        ListenToPress();
    }

    private void ListenToPress()
    {
        var t = target as TileSpawner;
        Event e = Event.current;
        switch (e.type)
        {
            case EventType.KeyDown:
                switch (e.keyCode)
                {
                    case KeyCode.Keypad8:
                        t.Rotation = Rotation.North;
                        break;
                    case KeyCode.Keypad2:
                        t.Rotation = Rotation.South;
                        break;
                    case KeyCode.Keypad4:
                        t.Rotation = Rotation.West;
                        break;
                    case KeyCode.Keypad6:
                        t.Rotation = Rotation.East;
                        break;
                }

                break;
        }
    }
}
