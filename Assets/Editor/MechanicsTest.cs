using System.Collections.Generic;
using Entitas;
using NUnit.Framework;
using UnityEngine;

[TestFixture]
public class MechanicsTest
{
    [Test]
    public void MoveRowTest()
    {
        var pools = new Pools();
        var core = Pools.CreateCorePool();
        pools.core = core;
        pools.uI = Pools.CreateUIPool();

        var tile00 = core.CreateEntity().AddPosition(0, 0).AddTileView(null);
        var tile10 = core.CreateEntity().AddPosition(1, 0).AddTileView(null);
        var tile01 = core.CreateEntity().AddPosition(0, 1).AddTileView(null);
        var tile11 = core.CreateEntity().AddPosition(1, 1).AddTileView(null);

        var moveOrder = core.CreateEntity().IsMove(true).AddDistance(1).AddRotation(Rotation.East).AddPosition(2, 0);

        var expected = new Vector3[]
                       {
                           new Vector3(1, 0),
                           new Vector3(2, 0),
                           new Vector3(0, 1),
                           new Vector3(1, 1),
                       };

        var moveSystem = (core.CreateSystem(new MoveLineSystem(), pools) as ReactiveSystem).subsystem;
        moveSystem.Execute(new List<Entity>() {moveOrder});
        
        Assert.AreEqual(expected, new []
                                  {
                                      new Vector3(tile00.position.X, tile00.position.Y),
                                      new Vector3(tile10.position.X, tile10.position.Y),
                                      new Vector3(tile01.position.X, tile01.position.Y),
                                      new Vector3(tile11.position.X, tile11.position.Y),
                                  });
    }

    [Test]
    public void MoveColumnTest()
    {
        var pools = new Pools();
        var core = Pools.CreateCorePool();
        pools.core = core;
        pools.uI = Pools.CreateUIPool();

        var tile00 = core.CreateEntity().AddPosition(0, 0).AddTileView(null);
        var tile10 = core.CreateEntity().AddPosition(1, 0).AddTileView(null);
        var tile01 = core.CreateEntity().AddPosition(0, 1).AddTileView(null);
        var tile11 = core.CreateEntity().AddPosition(1, 1).AddTileView(null);

        var moveOrder = core.CreateEntity().IsMove(true).AddDistance(2).AddRotation(Rotation.South).AddPosition(1, 0);

        var expected = new Vector3[]
                       {
                           new Vector3(0, 0),
                           new Vector3(1, -2),
                           new Vector3(0, 1),
                           new Vector3(1, -1),
                       };

        var moveSystem = (core.CreateSystem(new MoveLineSystem(), pools) as ReactiveSystem).subsystem;
        moveSystem.Execute(new List<Entity>() { moveOrder });

        Assert.AreEqual(expected, new[]
                                  {
                                      new Vector3(tile00.position.X, tile00.position.Y),
                                      new Vector3(tile10.position.X, tile10.position.Y),
                                      new Vector3(tile01.position.X, tile01.position.Y),
                                      new Vector3(tile11.position.X, tile11.position.Y),
                                  });
    }
}