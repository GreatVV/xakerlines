//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGenerator.ComponentExtensionsGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using Entitas;

namespace Entitas {

    public partial class Entity {

        public RotateableComponent rotateable { get { return (RotateableComponent)GetComponent(CoreComponentIds.Rotateable); } }
        public bool hasRotateable { get { return HasComponent(CoreComponentIds.Rotateable); } }

        public Entity AddRotateable(RotationDirection newDirection) {
            var component = CreateComponent<RotateableComponent>(CoreComponentIds.Rotateable);
            component.Direction = newDirection;
            return AddComponent(CoreComponentIds.Rotateable, component);
        }

        public Entity ReplaceRotateable(RotationDirection newDirection) {
            var component = CreateComponent<RotateableComponent>(CoreComponentIds.Rotateable);
            component.Direction = newDirection;
            ReplaceComponent(CoreComponentIds.Rotateable, component);
            return this;
        }

        public Entity RemoveRotateable() {
            return RemoveComponent(CoreComponentIds.Rotateable);
        }
    }
}

    public partial class CoreMatcher {

        static IMatcher _matcherRotateable;

        public static IMatcher Rotateable {
            get {
                if(_matcherRotateable == null) {
                    var matcher = (Matcher)Matcher.AllOf(CoreComponentIds.Rotateable);
                    matcher.componentNames = CoreComponentIds.componentNames;
                    _matcherRotateable = matcher;
                }

                return _matcherRotateable;
            }
        }
    }
