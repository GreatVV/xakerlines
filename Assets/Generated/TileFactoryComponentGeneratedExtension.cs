//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGenerator.ComponentExtensionsGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using Entitas;

public class TileFactoryComponent : IComponent {

    public TileFactory value;
}

namespace Entitas {

    public partial class Entity {

        public TileFactoryComponent tileFactory { get { return (TileFactoryComponent)GetComponent(SingletonesComponentIds.TileFactory); } }
        public bool hasTileFactory { get { return HasComponent(SingletonesComponentIds.TileFactory); } }

        public Entity AddTileFactory(TileFactory newValue) {
            var component = CreateComponent<TileFactoryComponent>(SingletonesComponentIds.TileFactory);
            component.value = newValue;
            return AddComponent(SingletonesComponentIds.TileFactory, component);
        }

        public Entity ReplaceTileFactory(TileFactory newValue) {
            var component = CreateComponent<TileFactoryComponent>(SingletonesComponentIds.TileFactory);
            component.value = newValue;
            ReplaceComponent(SingletonesComponentIds.TileFactory, component);
            return this;
        }

        public Entity RemoveTileFactory() {
            return RemoveComponent(SingletonesComponentIds.TileFactory);
        }
    }

    public partial class Pool {

        public Entity tileFactoryEntity { get { return GetGroup(SingletonesMatcher.TileFactory).GetSingleEntity(); } }
        public TileFactoryComponent tileFactory { get { return tileFactoryEntity.tileFactory; } }
        public bool hasTileFactory { get { return tileFactoryEntity != null; } }

        public Entity SetTileFactory(TileFactory newValue) {
            if(hasTileFactory) {
                throw new EntitasException("Could not set tileFactory!\n" + this + " already has an entity with TileFactoryComponent!",
                    "You should check if the pool already has a tileFactoryEntity before setting it or use pool.ReplaceTileFactory().");
            }
            var entity = CreateEntity();
            entity.AddTileFactory(newValue);
            return entity;
        }

        public Entity ReplaceTileFactory(TileFactory newValue) {
            var entity = tileFactoryEntity;
            if(entity == null) {
                entity = SetTileFactory(newValue);
            } else {
                entity.ReplaceTileFactory(newValue);
            }

            return entity;
        }

        public void RemoveTileFactory() {
            DestroyEntity(tileFactoryEntity);
        }
    }
}

    public partial class SingletonesMatcher {

        static IMatcher _matcherTileFactory;

        public static IMatcher TileFactory {
            get {
                if(_matcherTileFactory == null) {
                    var matcher = (Matcher)Matcher.AllOf(SingletonesComponentIds.TileFactory);
                    matcher.componentNames = SingletonesComponentIds.componentNames;
                    _matcherTileFactory = matcher;
                }

                return _matcherTileFactory;
            }
        }
    }
