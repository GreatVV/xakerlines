//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGenerator.ComponentExtensionsGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using Entitas;

namespace Entitas {

    public partial class Entity {

        static readonly TurnMadeComponent turnMadeComponent = new TurnMadeComponent();

        public bool isTurnMade {
            get { return HasComponent(UIComponentIds.TurnMade); }
            set {
                if(value != isTurnMade) {
                    if(value) {
                        AddComponent(UIComponentIds.TurnMade, turnMadeComponent);
                    } else {
                        RemoveComponent(UIComponentIds.TurnMade);
                    }
                }
            }
        }

        public Entity IsTurnMade(bool value) {
            isTurnMade = value;
            return this;
        }
    }
}

    public partial class UIMatcher {

        static IMatcher _matcherTurnMade;

        public static IMatcher TurnMade {
            get {
                if(_matcherTurnMade == null) {
                    var matcher = (Matcher)Matcher.AllOf(UIComponentIds.TurnMade);
                    matcher.componentNames = UIComponentIds.componentNames;
                    _matcherTurnMade = matcher;
                }

                return _matcherTurnMade;
            }
        }
    }
