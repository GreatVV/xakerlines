//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGenerator.ComponentExtensionsGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using Entitas;

namespace Entitas {

    public partial class Entity {

        public MaxComponent max { get { return (MaxComponent)GetComponent(CoreComponentIds.Max); } }
        public bool hasMax { get { return HasComponent(CoreComponentIds.Max); } }

        public Entity AddMax(float newValue) {
            var component = CreateComponent<MaxComponent>(CoreComponentIds.Max);
            component.Value = newValue;
            return AddComponent(CoreComponentIds.Max, component);
        }

        public Entity ReplaceMax(float newValue) {
            var component = CreateComponent<MaxComponent>(CoreComponentIds.Max);
            component.Value = newValue;
            ReplaceComponent(CoreComponentIds.Max, component);
            return this;
        }

        public Entity RemoveMax() {
            return RemoveComponent(CoreComponentIds.Max);
        }
    }
}

    public partial class CoreMatcher {

        static IMatcher _matcherMax;

        public static IMatcher Max {
            get {
                if(_matcherMax == null) {
                    var matcher = (Matcher)Matcher.AllOf(CoreComponentIds.Max);
                    matcher.componentNames = CoreComponentIds.componentNames;
                    _matcherMax = matcher;
                }

                return _matcherMax;
            }
        }
    }
