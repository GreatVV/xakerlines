//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGenerator.ComponentIndicesGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public static class UIComponentIds {

    public const int Enemy = 0;
    public const int Player = 1;
    public const int StartTile = 2;
    public const int ITileConnectionDrawer = 3;
    public const int RayListener = 4;
    public const int TurnLeftListener = 5;
    public const int TurnMade = 6;

    public const int TotalComponents = 7;

    public static readonly string[] componentNames = {
        "Enemy",
        "Player",
        "StartTile",
        "ITileConnectionDrawer",
        "RayListener",
        "TurnLeftListener",
        "TurnMade"
    };

    public static readonly System.Type[] componentTypes = {
        typeof(EnemyComponent),
        typeof(PlayerComponent),
        typeof(StartTileComponent),
        typeof(ITileConnectionDrawerComponent),
        typeof(RayListenerComponent),
        typeof(TurnLeftListenerComponent),
        typeof(TurnMadeComponent)
    };
}
