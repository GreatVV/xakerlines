using System.IO;
#if UNITY_WSA
#else
using System.Runtime.Serialization.Formatters.Binary;
#endif
using Entitas.Serialization.Blueprints;
using UnityEngine;

namespace Entitas.Unity.Serialization.Blueprints {

    [CreateAssetMenu(menuName = "Entitas/Blueprint", fileName = "Assets/New Blueprint.asset")]
    public class BinaryBlueprint : ScriptableObject {

        public byte[] blueprintData;
#if UNITY_WSA
#else
        readonly BinaryFormatter _serializer = new BinaryFormatter();
#endif
        public Blueprint Deserialize() {
            Blueprint blueprint = null;
            if(blueprintData == null || blueprintData.Length == 0) {
                blueprint = new Blueprint(string.Empty, "New Blueprint", new Entity(0, null, null));
            } else {
                using (var stream = new MemoryStream(blueprintData)) {
                    #if UNITY_WSA
#else
                    blueprint = (Blueprint)_serializer.Deserialize(stream);
#endif
                }
            }
#if !UNITY_WSA
            name = blueprint.name;
#endif
            return blueprint;
        }

        public void Serialize(Entity entity) {
            var blueprint = new Blueprint(entity.poolMetaData.poolName, name, entity);
            Serialize(blueprint);
        }

        public void Serialize(Blueprint blueprint) {
            using (var stream = new MemoryStream()) {
#if !UNITY_WSA
                _serializer.Serialize(stream, blueprint);
#endif
                blueprintData = stream.ToArray();
            }
        }
    }
}
