using System.Collections.Generic;
using System.Linq;
using Entitas;

public class ActivateTilesIfStartRayOnActivatorSystem : IReactiveSystem, ISetPools
{
    private Group _activators;

    public void Execute(List<Entity> entities)
    {
        var rayEntities = entities.SelectMany(x => x.rayDescription.value.Points).ToArray();
        for (int index = 0; index < _activators.GetEntities().Length; index++)
        {
            var entity = _activators.GetEntities()[index];
            var isBlocked = !rayEntities.Contains(entity);
            foreach (var tile in entity.connectedTiles.Tiles)
            {
                tile.IsBlocked(isBlocked);
            }
        }
    }

    public TriggerOnEvent trigger
    {
        get
        {
            return Matcher.AllOf(CoreMatcher.RayDescription, CoreMatcher.StartTile).OnEntityAdded();
        }
    }

    public void SetPools(Pools pools)
    {
        _activators = pools.core.GetGroup(Matcher.AllOf(CoreMatcher.Tile, CoreMatcher.Activator, CoreMatcher.ConnectedTiles));
    }
}