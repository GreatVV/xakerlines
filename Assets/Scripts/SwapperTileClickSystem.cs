using System.Collections.Generic;
using System.Linq;
using Entitas;

public class SwapperTileClickSystem : IReactiveSystem, ISetPools
{
    private Pool _uiPool;

    public void Execute(List<Entity> entities)
    {
        var swapper = entities.SingleEntity();
        var otherTile = swapper.connectedTiles.Tiles.First();
        var positionX = swapper.position.X;
        var positionY = swapper.position.Y;
        swapper.ReplacePosition(otherTile.position.X, otherTile.position.Y);
        otherTile.ReplacePosition(positionX, positionY);
        GameUtility.MakeTurn(_uiPool);
    }

    public TriggerOnEvent trigger
    {
        get
        {
            return Matcher.AllOf(CoreMatcher.SwapperTile, CoreMatcher.Clicked).OnEntityAdded();
        }
    }

    public void SetPools(Pools pools)
    {
        _uiPool = pools.uI;
    }
}