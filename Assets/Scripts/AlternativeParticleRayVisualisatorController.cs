using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AlternativeParticleRayVisualisatorController : RayVisualisator
{
    [SerializeField]
    private CombineParticleRay _particleSystemPrefab;

    [SerializeField]
    private GameObject _finishParticlePrefab;

    private GameObject _finishParticle;

    [SerializeField]
    private ParticleSystem _intersectionRaysPrefab;


    [SerializeField]
    private List<CombineParticleRay> _particleSystems;

    public static void UpdateRay(CombineParticleRay particleSystem, List<Segment> points)
    {
        particleSystem.UpdateRay(points);
    }

    public override void UpdateRay(List<List<Segment>> segments)
    {
        for (int i = 0; i < segments.Count; i++)
        {
            var targetPoints = segments[i];
            var lineRenderer = _particleSystems.ElementAtOrDefault(i);
            if (lineRenderer == null)
            {
                lineRenderer = Instantiate(_particleSystemPrefab, transform, false);
                _particleSystems.Add(lineRenderer);
            }
            lineRenderer.gameObject.SetActive(true);
            UpdateRay(lineRenderer, targetPoints);
        }
        for (int i = segments.Count; i < _particleSystems.Count; i++)
        {
            _particleSystems[i].gameObject.SetActive(false);
        }

        if (!_finishParticle)
        {
            _finishParticle = Instantiate(_finishParticlePrefab, transform, false);
        }
        _finishParticle.transform.localPosition = segments.Last().Last().End;
    }

    public override void ResetRay()
    {
        for (int i = 0; i < _particleSystems.Count; i++)
        {
            _particleSystems[i].gameObject.SetActive(false);
        }
        if (_finishParticle)
        {
            _finishParticle.gameObject.SetActive(false);
        }
    }
}