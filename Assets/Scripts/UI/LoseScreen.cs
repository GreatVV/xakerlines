﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseScreen : MonoBehaviour {

    public void Show(bool state)
    {
        gameObject.SetActive(state);
    }
}
