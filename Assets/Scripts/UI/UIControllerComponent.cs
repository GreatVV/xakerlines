﻿using Entitas;
using Entitas.CodeGenerator;

[Singletones, SingleEntity]
public class UIControllerComponent : IComponent
{
    public UIController Controller;
}

[UI, Core]
public class PlayerComponent : IComponent
{
    
}

[UI, Core]
public class EnemyComponent : IComponent
{
    
}