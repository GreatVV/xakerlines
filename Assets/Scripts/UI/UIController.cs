﻿using DG.Tweening;
using Entitas;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public WinScreen WinScreen;
    public LoseScreen LoseScreen;
    public GameScreen GameScreen;

    public LevelList LevelList;
    public GameObject Root;
    public Game Game;

    void OnValidate()
    {
        
        if (!WinScreen)
        {
            WinScreen = GetComponentInChildren<WinScreen>(true);
        }

        if (!LoseScreen)
        {
            LoseScreen = GetComponentInChildren<LoseScreen>(true);
        }
    }

    public void Restart()
    {
        WinScreen.Show(false);
        LoseScreen.Show(false);
        Game.RestartMap();
    }

    public void Init(Pools pools)
    {
        pools.singletones.SetUIController(this);
        GameScreen.Init(pools.uI);
    }

    public void StartMap(Map map)
    {
        LevelList.gameObject.SetActive(false);
        Root.gameObject.SetActive(true);
        Game.StartMap(map);
    }

    public void BackToMenu()
    {
        WinScreen.Show(false);
        LoseScreen.Show(false);
        Game.DestroyMap();
        Root.gameObject.SetActive(false);
        LevelList.gameObject.SetActive(true);
    }
}