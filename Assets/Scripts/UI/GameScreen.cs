using Entitas;
using UnityEngine;

public class GameScreen : MonoBehaviour
{
    public RayVisualisator[] PlayerRayRenderer;
    public RayVisualisator[] EnemyRayRenderer;
    public RayVisualisator[] FriendlyRayRenderer;
    public TextTurnLeftListener TurnLeftListener;

    void OnValidate()
    {
        if (!TurnLeftListener)
        {
            TurnLeftListener = GetComponentInChildren<TextTurnLeftListener>();
        }
    }

    public void Init(Pool uiPool)
    {
        foreach (var rayVisualisator in PlayerRayRenderer)
        {
            uiPool.CreateEntity().AddRayListener(rayVisualisator).IsPlayer(true).IsStartTile(true);
        }
        foreach (var rayVisualisator in EnemyRayRenderer)
        {
            uiPool.CreateEntity().AddRayListener(rayVisualisator).IsEnemy(true);
        }
        foreach (var rayVisualisator in FriendlyRayRenderer)
        {
            uiPool.CreateEntity().AddRayListener(rayVisualisator).IsPlayer(true);
        }
        uiPool.CreateEntity().AddTurnLeftListener(TurnLeftListener);
    }
}