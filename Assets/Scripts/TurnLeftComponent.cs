using Entitas;
using Entitas.CodeGenerator;

[Singletones, SingleEntity]
public class TurnLeftComponent : IComponent
{
    public int Turn;
}