using System;
using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;

public class VisualizeRaySystem : IReactiveSystem, ISetPools, IInitializeSystem, ICleanupSystem
{
    private Pool _uiPool;
    private Pool _corePool;
    private Group _playerRayListeners;
    private Group _enemyRayListeners;
    private Group _friendlyRayListeners;

    private Group _rayPoints;
    private Group _enemyRays;
    private Group _startPlayerEmitter;
    private Group _friendlyRayEmitter;
    private Group _tiles;
    private Group _nonRayTiles;
    private Group _rayListeners;
    private Pool _singletones;
    private Group _blocked;

    public void Execute(List<Entity> entities)
    {
        DropRays();
        //todo rewrite this with structure to describe whole ray

        foreach (var enemyRay in _enemyRays.GetEntities())
        {
            var tiles = _tiles.GetEntities();
            var rayDescription = DrawRay(tiles, enemyRay, _enemyRayListeners.GetEntities());
            enemyRay.ReplaceRayDescription(rayDescription);
            for (int i = 0; i < rayDescription.Points.Count; i++)
            {
                rayDescription.Points[i].ReplaceRayPoint(i, enemyRay);
            }
        }

        foreach (var startPoint in _startPlayerEmitter.GetEntities())
        {
            var rayDescription = DrawRay(_nonRayTiles.GetEntities(), startPoint, _playerRayListeners.GetEntities());
            startPoint.ReplaceRayDescription(rayDescription);
            for (int i = 0; i < rayDescription.Points.Count; i++)
            {
                rayDescription.Points[i].ReplaceRayPoint(i, startPoint);
            }
        }

        foreach (var friendlyRay in _friendlyRayEmitter.GetEntities())
        {
            var rayDescription = DrawRay(_nonRayTiles.GetEntities(), friendlyRay, _friendlyRayListeners.GetEntities());
            friendlyRay.ReplaceRayDescription(rayDescription);
            for (int i = 0; i < rayDescription.Points.Count; i++)
            {
                rayDescription.Points[i].ReplaceRayPoint(i, friendlyRay);
            }
        }
    }

    private void DropRays()
    {
        foreach (var rayPoint in _rayPoints.GetEntities())
        {
            rayPoint.RemoveRayPoint();
        }
    }

    public static RayDescription FindRay(Entity[] tiles, Entity startPoint)
    {
        var rayEntities = CalculateRayEntities(tiles, startPoint);
        var rayPoints = DropToSegments(rayEntities);
        var raySegments = MarkSegments(rayPoints);
        return new RayDescription()
               {
                   Points = rayEntities,
                   Segments = raySegments
               };
    }

    private static RayDescription DrawRay(Entity[] tiles, Entity startPoint, Entity[] listeners)
    {
        var description = FindRay(tiles, startPoint);
        foreach (var rayListener in listeners)
        {
            rayListener.rayListener.Listener.UpdateRay(description.Segments);
        }
        return description;
    }

    public TriggerOnEvent trigger
    {
        get
        {
            return UIMatcher.TurnMade.OnEntityAdded();
        }
    }

    public void SetPools(Pools pools)
    {
        _uiPool = pools.uI;
        _corePool = pools.core;
        _rayListeners = _uiPool.GetGroup(UIMatcher.RayListener);
        _playerRayListeners = _uiPool.GetGroup(Matcher.AllOf(UIMatcher.RayListener, UIMatcher.Player, UIMatcher.StartTile));
        _enemyRayListeners = _uiPool.GetGroup(Matcher.AllOf(UIMatcher.RayListener, UIMatcher.Enemy));
        _friendlyRayListeners = _uiPool.GetGroup(Matcher.AllOf(UIMatcher.RayListener, UIMatcher.Player).NoneOf(UIMatcher.StartTile));
        _rayPoints = _corePool.GetGroup(Matcher.AllOf(CoreMatcher.RayPoint).NoneOf(CoreMatcher.RayEmmiter));
        _enemyRays = _corePool.GetGroup(Matcher.AllOf(CoreMatcher.RayEmmiter, CoreMatcher.Enemy));
        _startPlayerEmitter = _corePool.GetGroup(Matcher.AllOf(CoreMatcher.Player, CoreMatcher.StartTile));
        _friendlyRayEmitter = _corePool.GetGroup(Matcher.AllOf(CoreMatcher.Player, CoreMatcher.RayEmmiter).NoneOf(CoreMatcher.StartTile));
        _tiles = _corePool.GetGroup(Matcher.AllOf(CoreMatcher.Tile, CoreMatcher.Position));
        _nonRayTiles = _corePool.GetGroup(Matcher.AllOf(CoreMatcher.Tile, CoreMatcher.Position).NoneOf(CoreMatcher.RayPoint));
        _singletones = pools.singletones;
        pools.singletones.GetGroup(SingletonesMatcher.MapInstance).OnEntityAdded += OnMapInstanceCreated;
        _blocked = _corePool.GetGroup(CoreMatcher.Blocked);
        _blocked.OnEntityRemoved += BlockedOnOnEntityUpdated;
        _blocked.OnEntityAdded += BlockedOnOnEntityUpdated;
    }

    private void BlockedOnOnEntityUpdated(Group @group, Entity entity, int index, IComponent previousComponent)
    {
        Execute(null);
    }

    private void OnMapInstanceCreated(Group _, Entity __, int ____, IComponent _____)
    {
        Initialize();
    }

    public void Initialize()
    {
        ResetRays();
        Execute(new List<Entity>());
        
    }

    private void ResetRays()
    {
        foreach (var entity in _playerRayListeners.GetEntities())
        {
            entity.rayListener.Listener.ResetRay();
        }

        foreach (var entity in _enemyRayListeners.GetEntities())
        {
            entity.rayListener.Listener.ResetRay();
        }
    }

    public static List<Entity> CalculateRayEntities(Entity[] tiles, Entity startPoint)
    {
        Entity previous = null;
        var nextPoint = startPoint.GetTargetTile(null, tiles);

        var points = new List<Entity>() {startPoint};
        if (nextPoint != null && !nextPoint.isBlocked)
        {
            points.Add(nextPoint);
        }
        
        while (nextPoint != null && !nextPoint.isBlocked)
        {
            var nextNextTile = nextPoint.GetTargetTile(previous, tiles);
            previous = nextPoint;
            nextPoint = nextNextTile;
            if (nextPoint != null && !nextPoint.isBlocked)
            {
                if (points.Contains(nextPoint))
                {
                    break;
                }
                points.Add(nextPoint);
            }
        }

        return points;
    }

    public static List<List<Vector3>> DropToSegments(List<Entity> rayPoints)
    {
        var currentSegment = new List<Vector3>();
        currentSegment.Add(rayPoints.First().Position());
        var segments = new List<List<Vector3>>() {currentSegment};
        
        for (int i = 0; i < rayPoints.Count-1; i++)
        {
            var point = rayPoints[i];
            var next = rayPoints[i + 1];

            if (next != null)
            {
                var nextPosition = next.Position();

                if (segments.Any(x => x.Contains(nextPosition)))
                {
                    break;
                }

                if (point.isPortalTile)
                {
                    currentSegment = new List<Vector3>();
                    currentSegment.Add(point.connectedTiles.Tiles.First().Position());
                    currentSegment.Add(nextPosition);
                    segments.Add(currentSegment);
                }
                else
                {
                    currentSegment.Add(nextPosition);
                }
            }
            else
            {
                break;
            }
        }
        var lastTile = rayPoints[rayPoints.Count - 1];
        if (lastTile.hasDistance && lastTile.distance.Distance > 0)
        {
            var lastTilePosition = lastTile.TargetTilePosition(rayPoints[rayPoints.Count - 1]);
            currentSegment.Add(lastTilePosition);
        }

        return segments;
    }

    public static List<List<Segment>> MarkSegments(List<List<Vector3>> rayPoints)
    {
        var segments = new List<List<Segment>>();
        foreach (var segmentPoints in rayPoints)
        {
            var segment = GameUtility.MarkSegments(segmentPoints);
            segments.Add(segment);
        }
        return segments;
    }

    public void Cleanup()
    {
        _blocked.OnEntityRemoved -= BlockedOnOnEntityUpdated;
        _blocked.OnEntityAdded -= BlockedOnOnEntityUpdated;
        foreach (var entity in _rayListeners.GetEntities())
        {
            entity.rayListener.Listener.ResetRay();
        }
        _singletones.GetGroup(SingletonesMatcher.MapInstance).OnEntityAdded -= OnMapInstanceCreated;
    }
}