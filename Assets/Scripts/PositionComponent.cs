using Entitas;

[Core]
public class PositionComponent : IComponent
{
    public int X;
    public int Y;
}