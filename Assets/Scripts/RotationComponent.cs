using Entitas;

[Core]
public class RotationComponent : IComponent
{
    public Rotation Rotation;
}

[Core]
public class MinComponent : IComponent
{
    public float Value;
}

[Core]
public class MaxComponent : IComponent
{
    public float Value;
}

[Core]
public class DestroyComponent : IComponent
{
    
}
