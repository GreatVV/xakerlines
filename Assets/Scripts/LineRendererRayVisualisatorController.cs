using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;

public class LineRendererRayVisualisatorController : RayVisualisator
{
    [SerializeField]
    private LineRenderer _prefab;

    [SerializeField]
    private List<LineRenderer> _lineRenderers;
    
    public static void UpdateRay(LineRenderer lineRenderer, List<Segment> points)
    {
        lineRenderer.numPositions = points.Count + 1;
        for (int i = 0; i < points.Count; i++)
        {
            lineRenderer.SetPosition(i, points[i].Start);
        }
        lineRenderer.SetPosition(points.Count, points.Last().End);
    }
    
    public override void UpdateRay(List<List<Segment>> segments)
    {
        for (int i = 0; i < segments.Count; i++)
        {
            var targetPoints = segments[i];
            var lineRenderer = _lineRenderers.ElementAtOrDefault(i);
            if (lineRenderer == null)
            {
                lineRenderer = Instantiate(_prefab, transform, false);
                _lineRenderers.Add(lineRenderer);
            }
            lineRenderer.enabled = true;
            UpdateRay(lineRenderer, targetPoints);
        }
        for (int i = segments.Count; i < _lineRenderers.Count; i++)
        {
            _lineRenderers[i].enabled = false;
        }
    }

    public override void ResetRay()
    {
        for (int i = 0; i < _lineRenderers.Count; i++)
        {
            _lineRenderers[i].enabled = false;
        }
    }
}