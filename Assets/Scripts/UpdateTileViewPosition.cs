using System.Collections.Generic;
using Entitas;

public class UpdateTileViewPosition : IReactiveSystem
{
    public void Execute(List<Entity> entities)
    {
        foreach (var entity in entities)
        {
            entity.tileView.View.transform.position = entity.Position();
        }
    }

    public TriggerOnEvent trigger
    {
        get
        {
            return Matcher.AllOf(CoreMatcher.TileView, CoreMatcher.Position).OnEntityAdded();
        }
    }
}