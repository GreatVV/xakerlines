﻿using System.Linq;
using DG.Tweening;
using UnityEngine;

public class DistanceSwitcherTileView : TileView
{
    public SpriteRenderer SpriteRenderer;
    public Sprite[] Distances;

    [SerializeField, Range(0, 1)]
    private float _inactiveColor = 0.5f;

    protected override void OnSetEntity()
    {
        var rotation = _entity.rotation.Rotation;
        transform.localRotation = GameUtility.RotationToQuaternion(rotation);
        SpriteRenderer.sprite = Distances.ElementAtOrDefault(_entity.distance.Distance-1);
        if (!SpriteRenderer.sprite)
        {
            Debug.LogWarning("No sprite for distance switcher tile view: "+ _entity.distance.Distance);
        }

        SpriteRenderer.color = new Color(SpriteRenderer.color.r, SpriteRenderer.color.g, SpriteRenderer.color.b, _entity.hasRayPoint ? 1 : _inactiveColor);
    }
}