using DG.Tweening;
using Entitas;
using UniRx;
using UnityEngine;

public class MagicSpawnAnimation : MonoBehaviour
{
    [SerializeField]
    private float _minTime = 0.1f;

    [SerializeField]
    private float _maxTime = 1f;

    [SerializeField]
    private AnimationCurve _curve;

    void OnValidate()
    {
        if (_curve == null)
        {
            _curve = AnimationCurve.EaseInOut(0,0,1,1);
        }
    }

    public IObservable<TileView> MagicalSpawn(TileSpawner spawner, Entity entity, TileView prefab)
    {
        return Observable.Create<TileView>
            (
             observer =>
             {
                 var tileView = Instantiate(prefab, new Vector3(entity.position.X, entity.position.Y, 0), Quaternion.identity);
                 tileView.SetEntity(entity);
                 tileView.transform.SetParent(spawner.transform.parent, true);
                 tileView.name = name + "View";
                 tileView.transform.localScale = Vector3.zero;
                 entity.AddTileView(tileView);
                 var tween = tileView.transform.DOScale(Vector3.one, UnityEngine.Random.Range(_minTime, _maxTime)).SetEase(_curve).OnComplete
                     (
                      () =>
                      {
                          observer.OnNext(tileView);
                          observer.OnCompleted();
                      });
                 return Disposable.Create(
                                          () =>
                                          {
                                              tween.Kill();
                                          });
             });
    }
}