public enum TileTypeForUI
{
    Direction = 0,
    Start = 1,
    Finish = 2,
    Portal = 3,
    RayEmitter = 4,
    DistanceSwitcher = 5,
    Block = 6,
    Swapper = 7,
    FriendlyRay = 8,
    Activator = 9
}