using System.Collections.Generic;
using Entitas;

public class SwapTileSystem : IReactiveSystem, ISetPools
{
    private Group _tileForSwap;
    private Group _selection;
    private Pool _uiPool;
    public void Execute(List<Entity> entities)
    {
        var first = _tileForSwap.GetSingleEntity();
        var second = entities.SingleEntity();
        var positionX = first.position.X;
        var positionY = first.position.Y;
        first.ReplacePosition(second.position.X, second.position.Y);
        second.ReplacePosition(positionX, positionY);
        
        foreach (var entity in _selection.GetEntities())
        {
            entity.isSelected = false;
        }

        GameUtility.MakeTurn(_uiPool);
    }

    public TriggerOnEvent trigger
    {
        get
        {
            return Matcher.AllOf(CoreMatcher.Selected, CoreMatcher.Clicked).NoneOf(CoreMatcher.SwapperTile).OnEntityAdded();
        }
    }

    public void SetPools(Pools pools)
    {
        _uiPool = pools.uI;
        _selection = pools.core.GetGroup(CoreMatcher.Selected);
        _tileForSwap = pools.core.GetGroup(Matcher.AllOf(CoreMatcher.SwapperTile, CoreMatcher.Selected));
    }
}