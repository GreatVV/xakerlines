using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ParticleRayVisualisatorController : RayVisualisator
{
    [SerializeField]
    private ParticleRay _particleSystemPrefab;

    [SerializeField]
    private List<ParticleRay> _particleSystems;

    public static void UpdateRay(ParticleRay particleSystem, List<Segment> points)
    {
        particleSystem.UpdateRay(points);
    }

    public override void UpdateRay(List<List<Segment>> points)
    {
        for (int i = 0; i < points.Count; i++)
        {
            var targetPoints = points[i];
            var lineRenderer = _particleSystems.ElementAtOrDefault(i);
            if (lineRenderer == null)
            {
                lineRenderer = Instantiate(_particleSystemPrefab, transform, false);
                _particleSystems.Add(lineRenderer);
            }
            lineRenderer.gameObject.SetActive(true);
            UpdateRay(lineRenderer, targetPoints);
        }
        for (int i = points.Count; i < _particleSystems.Count; i++)
        {
            _particleSystems[i].gameObject.SetActive(false);
        }
    }

    public override void ResetRay()
    {
        for (int i = 0; i < _particleSystems.Count; i++)
        {
            _particleSystems[i].gameObject.SetActive(false);
        }
    }
}