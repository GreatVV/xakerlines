using UnityEngine;
using UnityEngine.Assertions;

public class FinishTileView : TileView
{
    public SpriteRenderer Renderer;

    public Sprite Normal;
    public Sprite Blocked;

    void Awake()
    {
        Assert.IsNotNull(Normal);
        Assert.IsNotNull(Blocked);
        Assert.IsNotNull(Renderer);
    }

    protected override void OnSetEntity()
    {
        base.OnSetEntity();
        SetBlocked(_entity.isBlocked);
    }

    private void SetBlocked(bool isBlocked)
    {
        Renderer.sprite = isBlocked ? Blocked : Normal;
    }
}