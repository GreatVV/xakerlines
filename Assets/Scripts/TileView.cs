﻿using Entitas;
using UnityEngine;
using UnityEngine.EventSystems;


[RequireComponent(typeof(BoxCollider))]
public class TileView : MonoBehaviour, IPointerClickHandler
{
    protected Entity _entity;

    public void SetEntity(Entity entity)
    {
        _entity = entity;
        OnSetEntity();
    }

    protected virtual void OnSetEntity()
    {
    }

    public virtual void OnPointerClick(PointerEventData eventData)
    {
        if (_entity != null)
        {
            _entity.isClicked = true;
        }
        else
        {
            Debug.LogWarning("Entity is null");
        }
    }
}