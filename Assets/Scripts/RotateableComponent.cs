using Entitas;

[Core]
public class RotateableComponent : IComponent
{
    public RotationDirection Direction;
}