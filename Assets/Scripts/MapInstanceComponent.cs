﻿using System.Collections.Generic;
using Entitas;
using Entitas.CodeGenerator;

[Singletones, SingleEntity]
public class MapInstanceComponent : IComponent
{
    public List<TileView> TileViews;
}