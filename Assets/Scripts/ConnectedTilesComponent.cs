using System.Collections.Generic;
using Entitas;

[Core]
public class ConnectedTilesComponent : IComponent
{
    public List<Entity> Tiles;
}