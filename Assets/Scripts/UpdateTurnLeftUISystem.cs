using System.Collections.Generic;
using Entitas;

public class UpdateTurnLeftUISystem : IReactiveSystem, ISetPools, IInitializeSystem
{
    private Group _listeners;
    private Pool _singletones;

    public void Execute(List<Entity> entities)
    {
        var turnLeft = entities.SingleEntity();
        foreach (var entity in _listeners.GetEntities())
        {
            entity.turnLeftListener.Listener.OnTurnLeftListener(turnLeft.turnLeft.Turn);
        }
    }

    public TriggerOnEvent trigger
    {
        get
        {
            return SingletonesMatcher.TurnLeft.OnEntityAdded();
        }
    }

    public void SetPools(Pools pools)
    {
        _listeners = pools.uI.GetGroup(UIMatcher.TurnLeftListener);
        _singletones = pools.singletones;
    }

    public void Initialize()
    {
        if (_singletones.hasTurnLeft)
        {
            foreach (var entity in _listeners.GetEntities())
            {
                entity.turnLeftListener.Listener.OnTurnLeftListener(_singletones.turnLeft.Turn);
            }
        }
    }
}