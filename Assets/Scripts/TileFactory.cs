using System;
using Entitas;
using Entitas.CodeGenerator;
using UniRx;
using UnityEngine;

[SingleEntity, Singletones]
public class TileFactory : MonoBehaviour
{

    public TileView Direction1Prefab;
    public TileView Direction2Prefab;
    public TileView Direction3Prefab;
    public TileView StartPrefab;
    public TileView FinishPrefab;
    public TileView PortalPrefab;
    public TileView EnemyRayEmitterPrefab;
    public TileView DistanceSwitcherPrefab;
    public TileView BlockerPrefab;
    public TileView SwapperPrefab;
    public TileView FriendlyRayEmitterPrefab;
    public TileView ActivatorPrefab;
    public MagicSpawnAnimation MagicSpawn;

    private IObservable<Entity> GetEntityFor(Pool corePool, TileSpawner tileSpawner)
    {
        return Observable.Start
            (
             () =>
             {
                 var entity = GameUtility.CreateTile(corePool, (int)tileSpawner.transform.position.x, (int)tileSpawner.transform.position.y);
                 entity.IsBlocked(tileSpawner.IsBlocked);
                 switch (tileSpawner.TileType)
                 {
                     case TileTypeForUI.Direction:
                         entity.IsDirectionTile(true);
                         entity.AddRotateable(tileSpawner.RotationDirection);

                         entity.AddDistance(tileSpawner.Distance);
                         entity.AddRotation(tileSpawner.Rotation);
                         break;
                     case TileTypeForUI.Start:
                         entity.isStartTile = true;
                         entity.isRayEmmiter = true;
                         entity.isPlayer = true;

                         entity.AddRotation(tileSpawner.Rotation);
                         entity.AddDistance(tileSpawner.Distance);
                         break;
                     case TileTypeForUI.Finish:
                         entity.isFinishTile = true;
                         break;
                     case TileTypeForUI.Portal:
                         entity.IsPortalTile(true);
                         if (tileSpawner.ConnectedTileSpawner.Length == 0)
                         {
                             throw new ArgumentException("You need a connected tile for portal at "+tileSpawner.transform.position);
                         }
                         break;
                     case TileTypeForUI.RayEmitter:
                         entity.isRayEmmiter = true;
                         entity.isEnemy = true;

                         entity.AddRotation(tileSpawner.Rotation);
                         entity.AddDistance(tileSpawner.Distance);
                         break;
                     case TileTypeForUI.DistanceSwitcher:
                         entity.AddDistanceSwitcherTile(tileSpawner._min, tileSpawner._max);

                         entity.AddRotation(tileSpawner.Rotation);
                         entity.AddDistance(tileSpawner.Distance);
                         break;
                     case TileTypeForUI.Block:
                         entity.isBlockTile = true;
                         break;
                     case TileTypeForUI.Swapper:
                         entity.isSwapperTile = true;
                         entity.AddDistance(tileSpawner.Distance);
                         entity.AddRotation(tileSpawner.Rotation);
                         if (tileSpawner.ConnectedTileSpawner.Length == 0)
                         {
                             throw new ArgumentException("You need a connected tile for swapper at " + tileSpawner.transform.position);
                         }
                         break;
                     case TileTypeForUI.FriendlyRay:
                         entity.isRayEmmiter = true;
                         entity.isPlayer = true;
                         entity.AddRotation(tileSpawner.Rotation);
                         entity.AddDistance(tileSpawner.Distance);
                         break;
                    case TileTypeForUI.Activator:
                         entity.isActivator = true;
                         entity.AddRotation(tileSpawner.Rotation);
                         entity.AddDistance(tileSpawner.Distance);
                         if (tileSpawner.ConnectedTileSpawner.Length == 0)
                         {
                             throw new ArgumentException("You need a connected tile for activator at " + tileSpawner.transform.position);
                         }
                         break;
                 }
                 return entity;
             },
             Scheduler.MainThread);
    }

    void OnValidate()
    {
        if (!MagicSpawn)
        {
            MagicSpawn = GetComponent<MagicSpawnAnimation>() ?? gameObject.AddComponent<MagicSpawnAnimation>();
        }
    }

    public IObservable<TileView> Spawn(Pool corePool, TileSpawner tileSpawner, Func<TileSpawner, Entity, TileView, IObservable<TileView>> spawn)
    {
        return GetEntityFor(corePool, tileSpawner).SelectMany(
                                                    entity =>
                                                    {
                                                        var tileView = GetPrefab(entity);
                                                        return spawn(tileSpawner, entity, tileView);
                                                    });
    }

    public IObservable<TileView> BasicSpawn(TileSpawner spawner, Entity entity, TileView prefab)
    {
        return Observable.Create<TileView>
            (
             observer =>
             {
                 var tileView = Instantiate(prefab, new Vector3(entity.position.X, entity.position.Y, 0), Quaternion.identity);
                 tileView.SetEntity(entity);
                 tileView.transform.SetParent(spawner.transform.parent, true);
                 tileView.name = name + "View";
                 entity.AddTileView(tileView);
                 observer.OnNext(tileView);
                 observer.OnCompleted();
                 return Disposable.Empty;
             });
    }

   


    public TileView GetPrefab(Entity entity)
    {
        if (entity.isSwapperTile)
        {
            return SwapperPrefab;
        }

        if (entity.isStartTile)
        {
            return StartPrefab;
        }

        if (entity.isActivator)
        {
            return ActivatorPrefab;
        }

        if (entity.isFinishTile)
        {
            return FinishPrefab;
        }

        if (entity.isPortalTile)
        {
            return PortalPrefab;
        }

        if (entity.isBlockTile)
        {
            return BlockerPrefab;
        }

        if (entity.isDirectionTile)
        {
            switch (entity.distance.Distance)
            {
                case 1:
                    return Direction1Prefab;
                case 2:
                    return Direction2Prefab;
                case 3:
                    return Direction2Prefab;

            }
        }

        if (entity.hasDistanceSwitcherTile)
        {
            return DistanceSwitcherPrefab;
        }

        if (entity.isRayEmmiter &&
            entity.isEnemy)
        {
            return EnemyRayEmitterPrefab;
        }

        if (entity.isRayEmmiter &&
           entity.isPlayer)
        {
            return FriendlyRayEmitterPrefab;
        }
        return null;
    }
    
    public TileView Spawn(Entity entity)
    {
        var prefab = GetPrefab(entity);
        if (prefab)
        {
            var tileView = Instantiate(prefab, new Vector3(entity.position.X, entity.position.Y, 0), Quaternion.identity);
            tileView.SetEntity(entity);
            return tileView;
        }
        else
        {
            Debug.LogError("Can't create prefab for "+entity);
        }
        return null;
    }
}