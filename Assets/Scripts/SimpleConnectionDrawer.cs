using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class SimpleConnectionDrawer : MonoBehaviour, ITileConnectionDrawer
{
    [SerializeField]
    private UnityEngine.LineRenderer _lineRendererPrefab;
    Dictionary<Vector3, Dictionary<Vector3, LineRenderer>> _lineRenderers = new Dictionary<Vector3, Dictionary<Vector3, LineRenderer>>();
    public void AddConnection(Vector3 first, Vector3 second)
    {
        Dictionary<Vector3,LineRenderer> lineRenderer = null;
        _lineRenderers.TryGetValue(first, out lineRenderer);
        if (lineRenderer == null)
        {
            lineRenderer = new Dictionary<Vector3, LineRenderer>();
        }
        LineRenderer targetRenderer = null;
        lineRenderer.TryGetValue(second, out targetRenderer);
        if (targetRenderer == null)
        {
            targetRenderer = Instantiate(_lineRendererPrefab, transform, false);
            lineRenderer.Add(second, targetRenderer);
        }

        targetRenderer.SetPosition(0, first);
        targetRenderer.SetPosition(1, second);
    }

    public void Init(Pool uiPool)
    {
        uiPool.CreateEntity().AddITileConnectionDrawer(this);
    }

    public void DestroyLines()
    {
        foreach (var lineRenderer in _lineRenderers)
        {
            foreach (var line in lineRenderer.Value)
            {
                Destroy(line.Value.gameObject);
            }
        }
    }
}