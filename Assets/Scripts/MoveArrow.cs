using System;
using Entitas;
using UnityEngine;
using UnityEngine.EventSystems;

public class MoveArrow : TileView
{
    public Rotation Rotation;
    public int Distance;

    public int Max;
    public int Min;

    protected override void OnSetEntity()
    {
        base.OnSetEntity();
        _entity.AddPosition((int) transform.localPosition.x, (int) transform.localPosition.y).AddTileView(this).AddMin(Min).AddMax(Max);
    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        switch (Rotation)
        {
            case Rotation.North:
                if (transform.localPosition.y + Distance > Max)
                {
                    return;
                }
                break;
            case Rotation.South:
                if (transform.localPosition.y - Distance < Min)
                {
                    return;
                }
                break;
            case Rotation.East:
                if (transform.localPosition.x + Distance > Max)
                    return;
                break;
            case Rotation.West:
                if (transform.localPosition.x - Distance < Min)
                {
                    return;
                }
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        var vector = GameUtility.RotationToVector2(Rotation) * Distance;
        transform.localPosition = transform.localPosition + (Vector3) vector;
        Pools.sharedInstance.core.CreateEntity().IsMove(true).AddRotation(Rotation).AddDistance(Distance).AddPosition((int) transform.localPosition.x, (int) transform.localPosition.y);
    }
}