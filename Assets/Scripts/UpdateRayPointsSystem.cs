using System.Collections.Generic;
using Entitas;

public class UpdateRayPointsSystem : IReactiveSystem, IEnsureComponents
{
    public void Execute(List<Entity> entities)
    {
        foreach (var entity in entities)
        {
            entity.tileView.View.SetEntity(entity);
        }
    }

    public TriggerOnEvent trigger {
        get
        {
            return Matcher.AllOf(CoreMatcher.TileView, CoreMatcher.RayPoint).OnEntityAddedOrRemoved();
        }
    }

    public IMatcher ensureComponents
    {
        get
        {
            return CoreMatcher.TileView;
        }
    }
}