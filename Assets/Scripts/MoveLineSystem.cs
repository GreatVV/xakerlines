using System;
using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;

public class MoveLineSystem : IReactiveSystem, ISetPool, ISetPools
{
    private Pool _core;
    private Pool _uiPool;
    private Group _tiles;

    public void Execute(List<Entity> entities)
    {
        foreach (var entity in entities)
        {
            var tiles = _tiles.GetEntities();

            var direction = GameUtility.RotationToVector2(entity.rotation.Rotation) * entity.distance.Distance;

            switch (entity.rotation.Rotation)
            {
                case Rotation.North:
                case Rotation.South:
                    for (int index = 0; index < tiles.Length; index++)
                    {
                        var tile = tiles[index];
                        if (tile.position.X == entity.position.X)
                        {
                            tile.ReplacePosition(tile.position.X, (int) (tile.position.Y + direction.y));
                        }
                    }
                    break;
                case Rotation.East:
                case Rotation.West:
                    for (int index = 0; index < tiles.Length; index++)
                    {
                        var tile = tiles[index];
                        if (tile.position.Y == entity.position.Y)
                        {
                            tile.ReplacePosition((int)(tile.position.X + direction.x), tile.position.Y );
                        }
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
          
            entity.isDestroy = true;
        }

        GameUtility.MakeTurn(_uiPool);
    }

    public TriggerOnEvent trigger
    {
        get
        {
            return
                Matcher.AllOf(CoreMatcher.Move, CoreMatcher.Position, CoreMatcher.Rotation, CoreMatcher.Distance)
                       .OnEntityAdded();
        }
    }

    public void SetPool(Pool pool)
    {
        _core = pool;
        _tiles = _core.GetGroup(Matcher.AllOf(CoreMatcher.Position, CoreMatcher.TileView));
    }

    public void SetPools(Pools pools)
    {
        _uiPool = pools.uI;
    }
}