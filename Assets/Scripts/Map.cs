﻿using System.Linq;
using Entitas;
using Entitas.CodeGenerator;
using UnityEngine;

[Singletones, SingleEntity]
public class Map : MonoBehaviour
{
    public TileSpawner[] TileSpawners;
    public TileView[] AdditionalTileViews;
    public int Turns = 4;

    [ContextMenu("Grab tileSpawner")]
    public void GrabTileSpawner()
    {
        TileSpawners = GetComponentsInChildren<TileSpawner>().OrderBy(spawner => spawner.transform.position.x).ThenBy(spawner => spawner.transform.position.y).ToArray();
        foreach (var tileSpawner in TileSpawners)
        {
            tileSpawner.name = string.Format
                ("{0}:Distance:{3}_({1},{2})",
                 tileSpawner.TileType,
                 tileSpawner.transform.position.x,
                 tileSpawner.transform.position.y,
                 tileSpawner.Distance
                 );
        }

        for (int i = 0; i < TileSpawners.Length; i++)
        {
            var tileSpawner = TileSpawners[i];
            if (tileSpawner.ConnectedTileSpawner.Any())
            {
                foreach (var connected in tileSpawner.ConnectedTileSpawner)
                {
                    tileSpawner.name += string.Format(" Connected with: {0}", connected.name);
                }
            }
            tileSpawner.transform.SetSiblingIndex(i);
        }
    }

    void OnValidate()
    {
        GrabTileSpawner();
        
        transform.position = Vector3.zero;
    }
}