using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;

public class TurnMadeSystem : IReactiveSystem, ISetPools
{
    private Pool _uiPool;
    private Pool _corePool;
    private Pool _singletones;
    private Group _playerStarts;

    public void Execute(List<Entity> entities)
    {
        var soundManager = _singletones.soundManager.Manager;
        soundManager.Source.PlayOneShot(soundManager.ClickClips.Random());
        _singletones.ReplaceTurnLeft(_singletones.turnLeft.Turn - 1);

        var win = CheckWin();
        if (win)
        {
            soundManager.Source.PlayOneShot(soundManager.WinSound);
            _singletones.uIController.Controller.WinScreen.gameObject.SetActive(true);
        }
        if (_singletones.turnLeft.Turn == 0 && !win)
        {
            _singletones.uIController.Controller.LoseScreen.gameObject.SetActive(true);
        }
    }

    private bool CheckWin()
    {
        return _playerStarts.GetEntities().Select(entity => entity.rayDescription.value).Any(ray => ray.Points.Any(x => x.isFinishTile && !x.isBlocked));
    }

    public TriggerOnEvent trigger
    {
        get
        {
            return UIMatcher.TurnMade.OnEntityAdded();
        }
    }

  

    public void SetPools(Pools pools)
    {
        _uiPool = pools.uI;
        _corePool = pools.core;
        _singletones = pools.singletones;
        _playerStarts = pools.core.GetGroup(CoreMatcher.StartTile);
    }
}