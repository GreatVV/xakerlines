using System.Collections.Generic;
using UnityEngine;

public class ParticleRay : MonoBehaviour
{
    public ParticleSystem System;
    public Mesh Mesh;
    public float Width = 0.2f;
    public float ParticleEmitionPerUnit = 100;

    public void UpdateRay(List<Segment> segments)
    {
        Mesh = Generate(segments, Width, Mesh);
        var shape = System.shape;
        shape.mesh = Mesh;
        shape.shapeType = ParticleSystemShapeType.Mesh;
        shape.meshShapeType = ParticleSystemMeshShapeType.Triangle;

        var emission = System.emission;
        emission.rateOverTime = ParticleEmitionPerUnit * CountLong(segments);
    }

    private static float CountLong(List<Segment> points)
    {
        var sum = 0f;
        for (int i = 0; i < points.Count; i++)
        {
            var point = points[i];
            sum += Vector3.Distance(point.Start, point.End);
        }
        return sum;
    }

    private static Mesh Generate(List<Segment> points, float width, Mesh mesh = null)
    {
        if (mesh == null)
        {
            mesh = new Mesh();
        }
        var vertices = new List<Vector3>();
        var triangles = new List<int>();
        for (int index = 0; index < points.Count; index++)
        {
            var point = points[index].Start;
            var nextPoint = points[index].End;
            //vertical
            if (point.x == nextPoint.x)
            {
                var vertex1 = new Vector3(point.x - width/2, point.y);
                var vertex2 = new Vector3(point.x + width/2, point.y);
                var vertex3 = new Vector3(point.x - width / 2, nextPoint.y);
                var vertex4 = new Vector3(point.x + width / 2, nextPoint.y);
                vertices.Add(vertex1);
                vertices.Add(vertex2);
                vertices.Add(vertex3);
                vertices.Add(vertex4);
                triangles.Add(vertices.Count - 4);
                triangles.Add(vertices.Count - 3);
                triangles.Add(vertices.Count - 2);

                triangles.Add(vertices.Count - 3);
                triangles.Add(vertices.Count - 2);
                triangles.Add(vertices.Count - 1);
            }
            else
            {
                var vertex1 = new Vector3(point.x, point.y - width / 2);
                var vertex2 = new Vector3(point.x, point.y + width / 2);
                var vertex3 = new Vector3(nextPoint.x, point.y - width / 2);
                var vertex4 = new Vector3(nextPoint.x, point.y + width / 2);
                vertices.Add(vertex1);
                vertices.Add(vertex2);
                vertices.Add(vertex3);
                vertices.Add(vertex4);

                triangles.Add(vertices.Count - 4);
                triangles.Add(vertices.Count - 3);
                triangles.Add(vertices.Count - 2);

                triangles.Add(vertices.Count - 3);
                triangles.Add(vertices.Count - 2);
                triangles.Add(vertices.Count - 1);
            }
        }
        mesh.Clear();
        mesh.SetVertices(vertices);
        mesh.SetTriangles(triangles, 0);
        mesh.UploadMeshData(false);
        return mesh;
    }
}