using Entitas;

[Core]
public class DistanceSwitcherTileComponent : IComponent
{
    public int Min = 1;
    public int Max = 3;
}