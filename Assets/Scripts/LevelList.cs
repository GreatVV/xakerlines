﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class LevelList : MonoBehaviour
{
    public UIController UIController;
    public List<Map> Maps;

    public MapButton MapButtonPrefab;

    public Transform Root;

    public void StartMap(Map map)
    {
        UIController.StartMap(map);
    }

    public void Start()
    {
        for (int index = 0; index < Maps.Count; index++)
        {
            var map = Maps[index];
            var button = Instantiate(MapButtonPrefab, Root, false);
            if (index == 0)
            {
                EventSystem.current.SetSelectedGameObject(button.gameObject);
            }
            button.SetMap(index + 1, map);
            button.Click += StartMap;
        }
    }
}