using System;
using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;
using UniRx;
using Object = UnityEngine.Object;

public class StartMap : IInitializeSystem, ISetPools, ICleanupSystem
{
    private Pools _pools;
    private Group _tiles;
    private IDisposable _subscrtiption;
    public void Initialize()
    {
        if (_pools.singletones.hasMapInstance)
        {
            var views = _pools.singletones.mapInstance.TileViews;
            foreach (var tileView in views)
            {
                Object.Destroy(tileView);
            }
            views.Clear();
        }

        var map = _pools.singletones.map.value;
        var tileFactory = _pools.singletones.tileFactory.value;
        _pools.singletones.ReplaceTurnLeft(map.Turns);

        var tiles = new List<TileView>();
        _subscrtiption = map.TileSpawners.Select(tileSpawner => tileFactory.Spawn(_pools.core, tileSpawner, tileFactory.MagicSpawn.MagicalSpawn))
            .Concat().Subscribe(tile => tiles.Add(tile),
                                Debug.LogException,
                                () =>
                                {
                                    foreach (var tileSpawner in map.TileSpawners)
                                    {
                                        var tile = GameUtility.GetTileFor(_tiles.GetEntities(), tileSpawner);
                                        tileSpawner.MakeConnections(_tiles.GetEntities(), tile);
                                    }

                                    foreach (var additionalTileView in map.AdditionalTileViews)
                                    {
                                        additionalTileView.SetEntity(_pools.core.CreateEntity());
                                    }
                                    _pools.singletones.SetMapInstance(tiles);
                                }
            );
    }

    public void SetPools(Pools pools)
    {
        _pools = pools;
        _tiles = pools.core.GetGroup(Matcher.AllOf(CoreMatcher.Tile, CoreMatcher.Position));
    }

    public void Cleanup()
    {
        _subscrtiption.Dispose();
    }
}