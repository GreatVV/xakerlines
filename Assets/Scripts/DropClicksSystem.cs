using System.Collections.Generic;
using Entitas;

public class DropClicksSystem : IReactiveSystem
{
    public void Execute(List<Entity> entities)
    {
        foreach (var entity in entities)
        {
            entity.isClicked = false;
        }
    }

    public TriggerOnEvent trigger
    {
        get
        {
            return CoreMatcher.Clicked.OnEntityAdded();
        }
    }
}