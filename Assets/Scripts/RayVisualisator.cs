using System.Collections.Generic;
using UnityEngine;

public abstract class RayVisualisator : MonoBehaviour, IRayListener
{
    public abstract void UpdateRay(List<List<Segment>> points);
    public abstract void ResetRay();
}