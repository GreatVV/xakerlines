﻿using System;
using DG.Tweening;
using UnityEngine;

public class DirectionTileView : TileView
{
    [SerializeField]
    private AnimationCurve _rotationCurve;

    [SerializeField]
    private float _rotationTime = 0.1f;

    [SerializeField]
    private SpriteRenderer _sprite;

    [SerializeField, Range(0, 1)]
    private float _inactiveColor = 0.5f;

    protected override void OnSetEntity()
    {
        var rotation = _entity.rotation.Rotation;
        transform.DOLocalRotate(GameUtility.RotationToQuaternion(rotation).eulerAngles, _rotationTime)
                 .SetEase(_rotationCurve);

        _sprite.color = new Color(_sprite.color.r, _sprite.color.g, _sprite.color.b, _entity.hasRayPoint ? 1 : _inactiveColor);
        _sprite.transform.DOScale(_entity.isBlocked ? Vector3.zero : Vector3.one, _rotationTime);
    }
}