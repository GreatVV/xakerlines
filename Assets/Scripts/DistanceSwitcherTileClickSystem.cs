using System.Collections.Generic;
using Entitas;

public class DistanceSwitcherTileClickSystem : IReactiveSystem, IEnsureComponents, ISetPools
{
    private Pool _uiPool;

    public void Execute(List<Entity> entities)
    {
        foreach (var entity in entities)
        {
            ChangeDistance(entity);
            if (entity.hasConnectedTiles)
            {
                foreach (var connectedTile in entity.connectedTiles.Tiles)
                {
                    ChangeDistance(connectedTile);
                }
            }
            GameUtility.MakeTurn(_uiPool);
        }
    }

    private static void ChangeDistance(Entity entity)
    {
        if (entity.hasDistance && entity.hasDistanceSwitcherTile && entity.hasTileView)
        {
            var distance = entity.distance.Distance;
            distance++;
            if (distance > entity.distanceSwitcherTile.Max)
            {
                distance = entity.distanceSwitcherTile.Min;
            }
            entity.ReplaceDistance(distance);
            entity.tileView.View.SetEntity(entity);
        }
    }

    public TriggerOnEvent trigger
    {
        get
        {
            return Matcher.AllOf(CoreMatcher.Tile, CoreMatcher.DistanceSwitcherTile, CoreMatcher.Clicked).OnEntityAdded();
        }
    }

    public IMatcher ensureComponents
    {
        get
        {
            return Matcher.AllOf(CoreMatcher.DistanceSwitcherTile, CoreMatcher.Distance, CoreMatcher.TileView);
        }
    }

    public void SetPools(Pools pools)
    {
        _uiPool = pools.uI;
    }
}