using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;

public class CombineParticleRay : MonoBehaviour
{
    [Serializable]
    public class DistanceParticle
    {
        public int Distance;
        public GameObject Instance;
    }

    [SerializeField]
    private GameObject _oneStepPrefab;

    [SerializeField]
    private GameObject _twoStepPrefab;
    [SerializeField]
    private GameObject _twoStepPrefabTop;

    [SerializeField]
    private GameObject _threeStepPrefab;

    [SerializeField]
    private GameObject _threeStepPrefabTop;


    private List<DistanceParticle> _particleInstances = new List<DistanceParticle>();

    public void UpdateRay(List<Segment> segment)
    {
        for (int index = 0; index < segment.Count; index++)
        {
            var currentSegment = segment[index];
            var current = segment[index].Start;
            var next = segment[index].End;

            GameObject prefab;
            var distance = Mathf.RoundToInt(Mathf.Abs(Vector3.Distance(current, next)));
            switch (distance)
            {
                case 1:
                    prefab = _oneStepPrefab;
                    break;
                case 2:
                    if (currentSegment.SegmentType == SegmentType.Top)
                    {
                        prefab = _twoStepPrefabTop;
                    }
                    else
                    {
                        prefab = _twoStepPrefab;
                    }
                    break;
                case 3:
                    if (currentSegment.SegmentType == SegmentType.Top)
                    {
                        prefab = _threeStepPrefabTop;
                    }
                    else
                    {
                        prefab = _threeStepPrefab;
                    }
                    break;
                default:
                    prefab = null;
                    break;
            }
            if (!prefab)
            {
                Debug.LogWarningFormat("No prefab for distance: {0} {1} {2}", distance, current, next);
                continue;
            }

            var particleGameObject = _particleInstances.ElementAtOrDefault(index);
            if (particleGameObject == null || particleGameObject.Distance != distance)
            {
                if (particleGameObject != null)
                {
                    Destroy(particleGameObject.Instance.gameObject);
                    _particleInstances.RemoveAt(index);
                }

                particleGameObject = new DistanceParticle()
                                     {
                                         Distance = distance,
                                         Instance = Instantiate(prefab, transform, false)
                                     }; 
                
                _particleInstances.Insert(index, particleGameObject);
            }

            particleGameObject.Instance.SetActive(true);
            particleGameObject.Instance.transform.localPosition = current;

            if (current.x == next.x)
            {
                if (current.y > next.y)
                {
                    particleGameObject.Instance.transform.localRotation = Quaternion.Euler(0, 0, -180);
                }
                else
                {
                    particleGameObject.Instance.transform.localRotation = Quaternion.Euler(0, 0, 0);
                }
            }
            else
            {
                if (current.x > next.x)
                {
                    particleGameObject.Instance.transform.localRotation = Quaternion.Euler(0, 0, 90);
                }
                else
                {
                    particleGameObject.Instance.transform.localRotation = Quaternion.Euler(0, 0, -90);
                }
            }
        }
       

        for (int index = segment.Count; index < _particleInstances.Count; index++)
        {
            _particleInstances[index].Instance.SetActive(false);
        }
    }
}