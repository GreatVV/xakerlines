﻿using System;
using Entitas;
using UnityEngine;
using UnityEngine.UI;

public class TextTurnLeftListener : MonoBehaviour, ITurnLeftListener
{
    [SerializeField]
    private Text _text;

    public void OnTurnLeftListener(int turnLeft)
    {
        _text.text = string.Format("Ходов осталось: {0}", turnLeft);
    }
}