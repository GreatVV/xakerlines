using System.Collections.Generic;
using Entitas;

public class RotatableTileClickSystem : IReactiveSystem, IEnsureComponents, ISetPools
{
    private Pool _uiPool;

    public void Execute(List<Entity> entities)
    {
        foreach (var entity in entities)
        {
            Rotate(entity);
            if (entity.hasConnectedTiles)
            {
                foreach (var tile in entity.connectedTiles.Tiles)
                {
                    Rotate(tile);
                }
            }
            GameUtility.MakeTurn(_uiPool);
        }
    }

    private static void Rotate(Entity entity)
    {
        if (entity.hasRotation && entity.hasRotateable && entity.hasTileView)
        {
            var rotation = entity.rotation.Rotation;
            if (entity.rotateable.Direction == RotationDirection.Clockwise)
            {
                rotation += 2;
            }
            else
            {
                rotation -= 2;
            }
            rotation = (Rotation) ((int) rotation % 8);
            if (rotation < 0)
            {
                rotation = 8 + rotation;
            }
            entity.ReplaceRotation(rotation);
            entity.tileView.View.SetEntity(entity);
        }
    }

    public TriggerOnEvent trigger
    {
        get
        {
            return Matcher.AllOf(CoreMatcher.Tile, CoreMatcher.Clicked).OnEntityAdded();
        }
    }

    public IMatcher ensureComponents
    {
        get
        {
            return Matcher.AllOf(CoreMatcher.Rotateable, CoreMatcher.Rotation, CoreMatcher.TileView);
        }
    }

    public void SetPools(Pools pools)
    {
        _uiPool = pools.uI;
    }
}