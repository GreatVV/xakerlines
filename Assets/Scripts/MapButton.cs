﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MapButton : MonoBehaviour
{
    public Text _text;

    private Map Map { get; set; }

    public void SetMap(int index, Map map)
    {
        Map = map;
        _text.text = index.ToString();
    }

    public event Action<Map> Click;

    public void OnClick()
    {
        if (Click != null)
        {
            Click(Map);
        }
    }
}