using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;

public static class GameUtility
{
    public static void MakeTurn(Pool uiPool)
    {
        uiPool.CreateEntity().IsTurnMade(true);
    }

    public static Entity CreateTile(Pool corePool, int x, int y)
    {
        return corePool.CreateEntity().IsTile(true).AddPosition(x, y);
    }

    public static Vector2 RotationToVector2(Rotation rotation)
    {
        switch (rotation)
        {
            case Rotation.North:
                return Vector2.up;
            case Rotation.NorthEast:
                return new Vector2(1,1).normalized;
            case Rotation.East:
                return Vector2.right;
            case Rotation.SouthEast:
                return new Vector2(1,-1).normalized;
            case Rotation.South:
                return Vector2.down;
            case Rotation.SouthWest:
                return new Vector2(-1, -1).normalized;
            case Rotation.West:
                return Vector2.left;
            case Rotation.NorthWest:
                return new Vector2(-1, 1).normalized;
            default:
                throw new ArgumentOutOfRangeException("rotation", rotation, null);
        }
    }

    public static Quaternion RotationToQuaternion(Rotation rotation)
    {
        switch (rotation)
        {
            case Rotation.North:
                return Quaternion.Euler(0, 0, 0);
            case Rotation.NorthEast:
                return Quaternion.Euler(0, 0, -45);
            case Rotation.East:
                return Quaternion.Euler(0, 0, -90);
            case Rotation.SouthEast:
                return Quaternion.Euler(0, 0, -135);
            case Rotation.South:
                return Quaternion.Euler(0, 0, 180);
            case Rotation.SouthWest:
                return Quaternion.Euler(0, 0, 135);
            case Rotation.West:
                return Quaternion.Euler(0, 0, 90);
            case Rotation.NorthWest:
                return Quaternion.Euler(0, 0, 45);
            default:
                throw new ArgumentOutOfRangeException("rotation", rotation, "Rotation must be ok!!!");
        }
    }

    public static T Random<T>(this IEnumerable<T> collection)
    {
        return collection.ElementAtOrDefault(UnityEngine.Random.Range(0, collection.Count()));
    }

    public static Entity GetTargetTile(this Entity entity, Entity previousEntity, Entity[] possibleTiles)
    {
        if (entity.isPortalTile)
        {
            return TileAt(possibleTiles, entity.connectedTiles.Tiles.First().Position() + entity.Position() - previousEntity.Position());
        }
        if (entity.hasPosition && entity.hasDistance && entity.hasRotation)
        {
            return TileAt(possibleTiles, TargetTilePosition(entity.position, entity.distance, entity.rotation));
        }
        return null;
    }

    public static Vector2 TargetTilePosition(this Entity entity, Entity previousEntity)
    {
        if (entity.isPortalTile)
        {
            return entity.connectedTiles.Tiles.First().Position() + entity.Position() - previousEntity.Position();
        }
        if (entity.hasPosition && entity.hasDistance && entity.hasRotation)
        {
            return TargetTilePosition(entity.position, entity.distance, entity.rotation);
        }
        return entity.Position();
    }

    public static Vector2 TargetTilePosition(PositionComponent position, DistanceComponent distanceComponent, RotationComponent rotation)
    {
        return new Vector2(position.X, position.Y) + distanceComponent.Distance * RotationToVector2(rotation.Rotation);
    }

    public static Vector2 Position(this Entity entity)
    {
        return entity.hasPosition ? new Vector2(entity.position.X, entity.position.Y) : Vector2.zero;
    }

    public static bool AnyTileAt(Pool core, Vector2 nextPosition)
    {
        var tiles = core.GetGroup(Matcher.AllOf(CoreMatcher.Tile, CoreMatcher.Position)).GetEntities();
        return tiles.Any(x => x.position.X == nextPosition.x && x.position.Y == nextPosition.y);
    }

    public static Entity TileAt(Entity[] tiles, Vector2 nextPosition)
    {
        return tiles.FirstOrDefault(x => x.position.X == nextPosition.x && x.position.Y == nextPosition.y);
    }

    public static Entity GetTileFor(Entity[] tiles, TileSpawner tileSpawner)
    {
        return TileAt(tiles, tileSpawner.transform.position);
    }

    public static bool IsLinesCross(Vector2 v1, Vector2 v2, Vector2 v3, Vector2 v4)
    {
        var x11 = v1.x;
        var y11 = v1.y;

        var x12 = v2.x;
        var y12 = v2.y;

        var x21 = v3.x;
        var y21 = v3.y;

        var x22 = v4.x;
        var y22 = v4.y;

        var maxx1 = x11 > x12 ? x11 : x12;
        var maxy1 = y11 > y12 ? y11 : y12;
        var minx1 = x11 < x12 ? x11 : x12;
        var miny1 = y11 < y12 ? y11 : y12;
        var maxx2 = x21 > x22 ? x21 : x22;
        var maxy2 = y21 > y22 ? y21 : y22;
        var minx2 = x21 < x22 ? x21 : x22;
        var miny2 = y21 < y22 ? y21 : y22;

        if (minx1 > maxx2 || maxx1 < minx2 || miny1 > maxy2 || maxy1 < miny2)
            return false;  // ������, ���� ����� ����� ���� ����� �������...


        float dx1 = x12 - x11, dy1 = y12 - y11; // ����� �������� ������ ����� �� ��� x � y
        float dx2 = x22 - x21, dy2 = y22 - y21; // ����� �������� ������ ����� �� ��� x � y
        float dxx = x11 - x21, dyy = y11 - y21;
        int div, mul;


        if ((div = (int)((dy2 * dx1 - dx2 * dy1))) == 0)
            return false; // ����� �����������...
        if (div > 0)
        {
            if ((mul = (int)(dx1 * dyy - dy1 * dxx)) < 0 || mul > div)
                return false; // ������ ������� ������������ �� ������ ���������...
            if ((mul = (int)(dx2 * dyy - dy2 * dxx)) < 0 || mul > div)
                return false; // ������ ������� ������������ �� ������ ���������...
        }

        if ((mul = -(int)(dx1 * dyy - dy1 * dxx)) < 0 || mul > -div)
            return false; // ������ ������� ������������ �� ������ ���������...
        if ((mul = -(int)(dx2 * dyy - dy2 * dxx)) < 0 || mul > -div)
            return false; // ������ ������� ������������ �� ������ ���������...

        return true;
    }

    static public IntersectionInfo? Intersection(Segment segment1, Segment segment2)
    {
        if (IsLinesCross(segment1.Start, segment1.End, segment2.Start, segment2.End))
        {
            return new IntersectionInfo(segment1, segment2, Intersection(segment1.Start, segment1.End, segment2.Start, segment2.End));
        }
        return null;
    }

    static public Vector3 Intersection(Vector3 A, Vector3 B, Vector3 C, Vector3 D)
    {
        float xo = A.x, yo = A.y, zo = A.z;
        float p = B.x - A.x, q = B.y - A.y, r = B.z - A.z;

        float x1 = C.x, y1 = C.y, z1 = C.z;
        float p1 = D.x - C.x, q1 = D.y - C.y, r1 = D.z - C.z;

        var x = (xo * q * p1 - x1 * q1 * p - yo * p * p1 + y1 * p * p1) /
            (q * p1 - q1 * p);
        var y = (yo * p * q1 - y1 * p1 * q - xo * q * q1 + x1 * q * q1) /
            (p * q1 - p1 * q);
        var z = (zo * q * r1 - z1 * q1 * r - yo * r * r1 + y1 * r * r1) /
            (q * r1 - q1 * r);
        return new Vector3(x, y, z);
    }

    public static Vector2? FindIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, bool withBorders)
    {
        var d = (p1.x - p2.x) * (p4.y - p3.y) - (p1.y - p2.y) * (p4.x - p3.x);
        var da = (p1.x - p3.x) * (p4.y - p3.y) - (p1.y - p3.y) * (p4.x - p3.x);
        var db = (p1.x - p2.x) * (p1.y - p3.y) - (p1.y - p2.y) * (p1.x - p3.x);

        var ta = da / d;
        var tb = db / d;

        if ( (ta > 0 &&
            ta < 1 &&
            tb > 0 &&
            tb < 1 && !withBorders) || (
            ta >= 0 &&
            ta <= 1 &&
            tb >= 0 &&
            tb <= 1 && withBorders
            )
            )
        {
            var dx = p1.x + ta * (p2.x - p1.x);
            var dy = p1.y + ta * (p2.y - p1.y);

            return new Vector2(dx, dy);
        }

        return null;
    }

    public static List<Segment> MarkSegments(IEnumerable<Vector3> points)
    {
        var segments = new Segment[points.Count() - 1];
        for (int index = 0; index < points.Count() - 1; index++)
        {
            var start = points.ElementAtOrDefault(index);
            var end = points.ElementAtOrDefault(index + 1);

            var segment = new Segment(index, SegmentType.Regular, start, end);
            segments[index] = segment;
        }

        for (int i = segments.Length-1; i > 0; i--)
        {
            var currentSegment = segments[i];
            for (int j = i-1; j >= 0; j--)
            {
                var nextSegment = segments[j];
                var longSegment = Vector2.Distance(nextSegment.Start, nextSegment.End) > 1;
                var intersection = FindIntersection(nextSegment, currentSegment, longSegment);
                if (intersection.HasValue && longSegment && Math.Abs(currentSegment.Order - nextSegment.Order) > 1)
                {
                    currentSegment.SegmentType = SegmentType.Top;
                    nextSegment.SegmentType = SegmentType.Under;
                    segments[j] = nextSegment;
                    segments[i] = currentSegment;
                    break;
                }
            }
        }
        return segments.ToList();
    }

    public static Vector2? FindIntersection(Segment segment1, Segment segment2, bool withBorders = false)
    {
        return FindIntersection(segment1.Start, segment1.End, segment2.Start, segment2.End, withBorders);
    }
}

public struct IntersectionInfo
{
    public readonly Segment Segment1;
    public readonly Segment Segment2;
    public readonly Vector3 Point;

    public IntersectionInfo(Segment first, Segment second, Vector3 point)
    {
        Segment1 = first;
        Segment2 = second;
        Point = point;
    }
}

public struct Segment : IEquatable<Segment>
{
    public SegmentType SegmentType;
    public readonly int Order;
    public Vector2 Start;
    public Vector2 End;

    public Segment(int order, SegmentType segmentType, Vector2 start, Vector2 end)
    {
        Order = order;
        SegmentType = segmentType;
        Start = start;
        End = end;
    }

    public bool Equals(Segment other)
    {
        return other.SegmentType == SegmentType && other.Order == Order && other.Start == Start && other.End == End;
    }

    public override string ToString()
    {
        return string.Format("{0} {1} {2} - {3} ", Order, SegmentType, Start, End);
    }
}

public enum SegmentType
{
    Regular,
    Under,
    Top
}
