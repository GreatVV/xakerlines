using System.Collections.Generic;

public interface IRayListener
{
    void UpdateRay(List<List<Segment>> segments);

    void ResetRay();
}