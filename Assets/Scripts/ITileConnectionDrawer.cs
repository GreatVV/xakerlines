using UnityEngine;

[UI]
public interface ITileConnectionDrawer
{
    void AddConnection(Vector3 first, Vector3 second);
}