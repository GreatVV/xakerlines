﻿using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public class SoundManager : MonoBehaviour
{
    public AudioClip[] ClickClips;
    public AudioClip WinSound;
    public AudioSource Source;

    public void Awake()
    {
        Assert.IsNotNull(WinSound);
        Assert.IsNotNull(Source);
        Assert.IsTrue(ClickClips.Any());
    }
}