﻿using Entitas;
using Entitas.CodeGenerator;

[Singletones, SingleEntity]
public class SoundManagerComponent : IComponent
{
    public SoundManager Manager;
}