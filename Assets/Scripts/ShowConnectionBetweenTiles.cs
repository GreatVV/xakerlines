using System.Collections.Generic;
using Entitas;

public class ShowConnectionBetweenTiles : IReactiveSystem, ISetPools
{
    private Group _drawer;

    public void Execute(List<Entity> entities)
    {
        foreach (var entity in entities)
        {
            foreach (var drawer in _drawer.GetEntities())
            {
                foreach (var tile in entity.connectedTiles.Tiles)
                {
                    drawer.iTileConnectionDrawer.value.AddConnection(entity.Position(), tile.Position());
                }
            }
        }
    }

    public TriggerOnEvent trigger
    {
        get
        {
            return CoreMatcher.ConnectedTiles.OnEntityAdded();
        }
    }

    public void SetPools(Pools pools)
    {
        _drawer = pools.uI.GetGroup(UIMatcher.ITileConnectionDrawer);
    }
}