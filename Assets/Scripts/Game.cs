﻿using Entitas;
using UnityEngine;
using UnityEngine.Assertions;

public class Game : MonoBehaviour
{
    public UIController Controller;
    private Systems _systems;
    public TileFactory TileFactory;
    public SoundManager SoundManager;
    public SimpleConnectionDrawer SimpleConnectionDrawer;
    private Map _map;
    private Pools _pools;
    private Map _currentMapPrefab;

    private void Awake()
    {
        Assert.IsNotNull(SoundManager);
        Assert.IsNotNull(SimpleConnectionDrawer);
        _pools = Pools.sharedInstance;
        _pools.SetAllPools();
        _pools.singletones.SetTileFactory(TileFactory);
        Controller.Init(_pools);
        SimpleConnectionDrawer.Init(_pools.uI);
        _pools.singletones.SetSoundManager(SoundManager);
        _systems = new Systems();
    }

    void OnValidate()
    {
        if (!SimpleConnectionDrawer)
        {
            SimpleConnectionDrawer = FindObjectOfType<SimpleConnectionDrawer>();
        }
    }

    public void StartMap(Map prefab)
    {
        if (!_pools.singletones.hasMap)
        {
            _currentMapPrefab = prefab;
            _map = Instantiate(prefab);
            _pools.singletones.SetMap(_map);
            _systems = CreateSystems(_pools);
            _systems.Initialize();
            _systems.ActivateReactiveSystems();
        }
    }

    public void DestroyMap()
    {
        if (_map)
        {
            Destroy(_map.gameObject);
        }
        _systems.Cleanup();
        _map = null;
        _systems.DeactivateReactiveSystems();
        _pools.core.Reset();
        _pools.singletones.RemoveMap();
        SimpleConnectionDrawer.DestroyLines();
        if (_pools.singletones.hasMapInstance)
        {
            _pools.singletones.RemoveMapInstance();
        }
    }

    private Systems CreateSystems(Pools pools)
    {
        return
            new Feature("Game").Add(pools.core.CreateSystem(new StartMap(), pools))
                               .Add(pools.core.CreateSystem(new RotatableTileClickSystem(), pools))
                               .Add(pools.core.CreateSystem(new DistanceSwitcherTileClickSystem(), pools))
                               .Add(pools.core.CreateSystem(new SwapperTileClickSystem(), pools))
                               .Add(pools.core.CreateSystem(new MoveLineSystem(), pools))
                               .Add(pools.core.CreateSystem(new DropClicksSystem(), pools))
                               .Add(pools.core.CreateSystem(new UpdateTileViewPosition(), pools))
                               .Add(pools.core.CreateSystem(new UpdateTileBlockingSystem(), pools))
                               .Add(pools.core.CreateSystem(new ShowConnectionBetweenTiles(), pools))
                               .Add(pools.singletones.CreateSystem(new UpdateTurnLeftUISystem(), pools))
                               .Add(pools.uI.CreateSystem(new VisualizeRaySystem(), pools))
                               .Add(pools.core.CreateSystem(new UpdateRayPointsSystem(), pools))
                               .Add(pools.core.CreateSystem(new ActivateTilesIfStartRayOnActivatorSystem(), pools))
                               .Add(pools.core.CreateSystem(new BlockIfNotFriendlyRaySystem(), pools))
                               .Add(pools.uI.CreateSystem(new TurnMadeSystem(), pools))
                               .Add(pools.core.CreateSystem(new DestroySystem(), pools));
    }

    private void Update()
    {
        _systems.Execute();
    }

    public void RestartMap()
    {
        DestroyMap();
        StartMap(_currentMapPrefab);
    }
}