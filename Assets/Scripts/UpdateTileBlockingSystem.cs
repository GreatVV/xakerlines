using System.Collections.Generic;
using Entitas;

public class UpdateTileBlockingSystem : IReactiveSystem
{
    public void Execute(List<Entity> entities)
    {
        foreach (var entity in entities)
        {
            entity.tileView.View.SetEntity(entity);
        }
    }

    public TriggerOnEvent trigger
    {
        get
        {
            return
                Matcher.AllOf(CoreMatcher.TileView, CoreMatcher.Blocked)
                       .OnEntityAddedOrRemoved();
        }
    }
}