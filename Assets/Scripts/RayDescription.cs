using System;
using System.Collections.Generic;
using Entitas;

[Core, Serializable]
public class RayDescription
{
    public List<List<Segment>> Segments;
    public List<Entity> Points;
}