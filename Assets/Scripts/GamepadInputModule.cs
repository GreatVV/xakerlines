using UnityEngine;
using UnityEngine.EventSystems;

public class GamepadInputModule : BaseInputModule
{
    public override void Process()
    {
        
    }

    protected override AxisEventData GetAxisEventData(float x, float y, float moveDeadZone)
    {
        Debug.LogFormat("Axis move: {0} {1}",x,y);

        return base.GetAxisEventData(x, y, moveDeadZone);
    }

    public override bool ShouldActivateModule()
    {
        return false;
    }
}