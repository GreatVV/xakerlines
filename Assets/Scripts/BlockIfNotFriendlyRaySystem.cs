using System.Collections.Generic;
using System.Linq;
using Entitas;

public class BlockIfNotFriendlyRaySystem : IReactiveSystem, ISetPools
{
    private Group _finishTiles;
    private Group _friendlyRay;
    private Group _startRay;

    public void Execute(List<Entity> startTileRay)
    {
        if (_friendlyRay.count == 0)
        {
            return;
        }

        var blocked = !(_startRay.GetEntities().SelectMany(x=>x.rayDescription.value.Segments).SelectMany(x=>x).Select(x=>x.End).Intersect
                    (_friendlyRay.GetEntities().SelectMany(x => x.rayDescription.value.Segments).SelectMany(x => x).Select(x => x.End)).Any());

        foreach (var entity in _finishTiles.GetEntities())
        {
            entity.IsBlocked(blocked);
        }
    }

    public TriggerOnEvent trigger
    {
        get
        {
            return CoreMatcher.RayDescription.OnEntityAdded();
        }
    }

    public void SetPools(Pools pools)
    {
        _finishTiles = pools.core.GetGroup(CoreMatcher.FinishTile);
        _friendlyRay = pools.core.GetGroup(Matcher.AllOf(CoreMatcher.RayEmmiter, CoreMatcher.Player).NoneOf(CoreMatcher.StartTile));
        _startRay = pools.core.GetGroup(Matcher.AllOf(CoreMatcher.StartTile));
    }
}