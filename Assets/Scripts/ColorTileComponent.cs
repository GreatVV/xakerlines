using Entitas;

[Core]
public class ColorTileComponent : IComponent
{
    
}

[Core, UI]
public class StartTileComponent : IComponent
{
    
}

[Core]
public class FinishTileComponent : IComponent
{

}

[Core]
public class RayPointComponent : IComponent
{
    public int Number;
    public Entity RayStart;
}

