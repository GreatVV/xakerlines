using System;
using System.Linq;
using Entitas;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;

public class TileSpawner : MonoBehaviour
{
    [FormerlySerializedAs("DirectionJump")]
    [SerializeField]
    public int Distance = 1;
    public TileTypeForUI TileType;
    public Rotation Rotation;
    public RotationDirection RotationDirection;
    public TileSpawner[] ConnectedTileSpawner;
    public bool IsBlocked;
    [SerializeField]
    [FormerlySerializedAs("_min")]
    public int _min = 1;

    [SerializeField]
    [FormerlySerializedAs("_max")]
    public int _max = 3;
   
    public void MakeConnections(Entity[] tiles, Entity entity)
    {
        var connectedTiles = ConnectedTileSpawner.Select(x => GameUtility.TileAt(tiles, x.transform.position)).Distinct().ToList();
        entity.AddConnectedTiles(connectedTiles);
    }

    void OnDrawGizmos()
    {
        var rotationDirection = GameUtility.RotationToVector2(Rotation) * 0.6f;
        Gizmos.DrawWireCube(transform.position, Vector3.one);
        switch (TileType)
        {
            case TileTypeForUI.FriendlyRay:
            case TileTypeForUI.Direction:
            case TileTypeForUI.RayEmitter:
            case TileTypeForUI.DistanceSwitcher:
            case TileTypeForUI.Swapper:
            case TileTypeForUI.Activator:
                Gizmos.DrawRay(transform.position - (Vector3)rotationDirection / 2, rotationDirection);
                Gizmos.DrawSphere(transform.position + (Vector3)rotationDirection / 2, 0.1f);
                break;
            case TileTypeForUI.Start:
            case TileTypeForUI.Finish:
            case TileTypeForUI.Portal:
            case TileTypeForUI.Block:
                Gizmos.DrawSphere(transform.position, 0.2f);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        

        foreach (var tileSpawner in ConnectedTileSpawner)
        {
            if (tileSpawner)
            {
                Gizmos.DrawLine(transform.position, tileSpawner.transform.position);
            }
        }
    }

    void OnValidate()
    {
        if (TileType == TileTypeForUI.Activator)
        {
            if (ConnectedTileSpawner.Any())
            {
                foreach (var tileSpawner in ConnectedTileSpawner)
                {
                    tileSpawner.IsBlocked = true;
                }
            }
        }
    }


}